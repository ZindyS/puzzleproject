package com.example.puzzle

import android.app.Activity
import android.graphics.Paint
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.MotionEvent
import android.view.View
import androidx.annotation.RequiresApi
import androidx.fragment.app.FragmentManager
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.view.*
import java.lang.Float.max
import java.lang.Float.min
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.abs
import kotlin.math.atan2
import kotlin.random.Random

class MainActivity : AppCompatActivity() {
    var lastRot1=0f
    var lastRot2=0f
    var lastRot3=0f
    var angle=0f
    var aboba=false
    var x=0f
    var y=0f
    var center=180
    var cntt=3
    var attemps=0
    var isStart=false
    var time = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        root=this
        smt()

        val handler = Handler()
        Thread {
            val runnable = object : Runnable {
                override fun run() {
                    Log.d("errror", isStart.toString())
                    if (isStart) {
                        runOnUiThread {
                            timeandattemps.text =
                                "${SimpleDateFormat("mm:ss").format(time*1000)}\n$attemps"
                        }
                        time++
                    }
                    handler.postDelayed(this, 1000)
                }
            }
            handler.post(runnable)
        }.start()
        button.setOnClickListener {
            startGame()
        }
    }

    fun endGame() {
        CustomAlert(attemps, Date(0, 0, 0, 0, 0, time)).show(supportFragmentManager, "1")
        isStart=false
        timeandattemps.visibility=View.GONE
    }

    fun startGame() {
        timeandattemps.visibility=View.VISIBLE
        timeandattemps.text = "00:00\n0"
        isStart=true
        attemps=0
        time=0
        button.isClickable = false
        button.animate()
            .setDuration(400)
            .translationY(-600f)
            .start()
        top5.animate()
            .setDuration(400)
            .translationX(700f)
            .start()
        place1.visibility= View.VISIBLE
        place2.visibility= View.VISIBLE
        place3.visibility= View.VISIBLE
        poly1.visibility= View.VISIBLE
        poly2.visibility= View.VISIBLE
        poly3.visibility= View.VISIBLE
        cntt=3

        val figure1 = mutableListOf(Pair(5f, 5f), Pair(240f, 240f))
        val figure2 = mutableListOf(Pair(5f, 5f))
        val figure3 = mutableListOf(Pair(5f, 5f))
        val rnd = Random(Date().time)
        for (i in 0..(abs(rnd.nextInt()%2)+2)) {
            figure1.add(Pair((5+abs(rnd.nextInt()%235)).toFloat(), abs(5+(rnd.nextInt()%235)).toFloat()))
        }
        for (i in 0..(abs(rnd.nextInt()%2)+2)) {
            figure2.add(Pair((5+abs(rnd.nextInt()%235)).toFloat(), (5+abs(rnd.nextInt()%235)).toFloat()))
        }
        for (i in 0..(abs(rnd.nextInt()%2)+2)) {
            figure3.add(Pair((5+abs(rnd.nextInt()%235)).toFloat(), (5+abs(rnd.nextInt()%235)).toFloat()))
        }
        Log.d("errror", figure1.toString())
        Log.d("errror", figure2.toString())
        Log.d("errror", figure3.toString())
        poly1.animate()
            .setDuration(0)
            .alpha(1f)
            .translationY(350f)
            .start()
        poly2.animate()
            .setDuration(0)
            .alpha(1f)
            .translationY(350f)
            .start()
        poly3.animate()
            .setDuration(0)
            .alpha(1f)
            .translationY(350f)
            .start()
        polyWHT1.animate()
            .setDuration(0)
            .alpha(1f)
            .start()
        polyWHT2.animate()
            .setDuration(0)
            .alpha(1f)
            .start()
        polyWHT3.animate()
            .setDuration(0)
            .alpha(1f)
            .start()
        place1.newTriangle(figure1, Paint.Style.FILL)
        place2.newTriangle(figure2, Paint.Style.FILL)
        place3.newTriangle(figure3, Paint.Style.FILL)
        place1.animate()
            .setDuration(0)
            .alpha(1f)
            //.translationX(rnd.nextInt(0, 800).toFloat())
            .rotation((rnd.nextInt(360)).toFloat())
            .start()
        place2.animate()
            .setDuration(0)
            .alpha(1f)
            //.translationX(rnd.nextInt(-400, 400).toFloat())
            .rotation((rnd.nextInt(360)).toFloat())
            .start()
        place3.animate()
            .setDuration(0)
            .alpha(1f)
            //.translationX(rnd.nextInt(-800, 0).toFloat())
            .rotation((rnd.nextInt(360)).toFloat())
            .start()


        polyWHT1.newTriangle(figure1, Paint.Style.STROKE)
        polyWHT2.newTriangle(figure2, Paint.Style.STROKE)
        polyWHT3.newTriangle(figure3, Paint.Style.STROKE)

        poly1.setOnTouchListener { view, motionEvent ->
            val action = motionEvent.actionMasked
            val cnt = motionEvent.pointerCount
            if (action == MotionEvent.ACTION_MOVE) {
                if (cnt==2) {
                    var result = atan2(center-motionEvent.getY(1), motionEvent.getX(1)-center)*180/3.14f
                    result=angle-result+lastRot1
                    Log.d("errror", "move $result, ${motionEvent.getY(1)}")
                    polyWHT1.animate()
                        .setDuration(0)
                        .rotation(result%360)
                        .start()
                } else {
                    if (!aboba) {
                        poly1.animate()
                            .setDuration(0)
                            .translationXBy(motionEvent.x - x)
                            .translationYBy(motionEvent.y - y)
                            .start()
                    } else {
                        x=motionEvent.x
                        y=motionEvent.y
                        aboba=false
                    }
                }
            } else if (action == MotionEvent.ACTION_DOWN) {
                aboba=false
                x=motionEvent.x
                y=motionEvent.y
                Log.d("errror", "$x, $y")
            } else if (action == MotionEvent.ACTION_POINTER_DOWN) {
                angle= atan2(center-motionEvent.getY(1), motionEvent.getX(1)-center)*180/3.14f
                aboba=true
            } else if (action==MotionEvent.ACTION_UP) {
                Log.d("errror", "up ${poly1.x-place1.x+50}, ${poly1.y}, ${abs((polyWHT1.rotation-place1.rotation)%350)}")
                lastRot1=polyWHT1.rotation
                if (abs(poly1.x-place2.x+50)<50 && abs(poly1.y)<50 ||
                    abs(poly1.x-place3.x+50)<50 && abs(poly1.y)<50) {
                    poly1.animate()
                        .setDuration(400)
                        .translationX(0f)
                        .translationY(350f)
                        .start()
                    polyWHT1.animate()
                        .setDuration(400)
                        .rotation(0f)
                        .start()
                    attemps++
                } else if (abs(poly1.x-place1.x+50)<50 && abs(poly1.y)<50) {
                    if (abs((polyWHT1.rotation-place1.rotation)%355)<10f) {
                        poly1.isEnabled = false
                        poly1.animate()
                            .setDuration(200)
                            .translationX(0f)
                            .translationY(0f)
                            .start()
                        polyWHT1.animate()
                            .setDuration(200)
                            .rotationBy((720+place1.rotation-polyWHT1.rotation)%360)
                            .withEndAction {
                                place1.animate()
                                    .setDuration(400)
                                    .alpha(0f)
                                    .withEndAction {
                                        polyWHT1.animate()
                                            .setDuration(200)
                                            .alpha(0f)
                                            .start()
                                        cntt--
                                        if (cntt==0) {
                                            endGame()
                                        }
                                    }
                                    .start()
                            }
                            .start()
                    } else {
                        poly1.animate()
                            .setDuration(400)
                            .translationX(0f)
                            .translationY(350f)
                            .start()
                        polyWHT1.animate()
                            .setDuration(400)
                            .rotation(ZeroOr360(polyWHT1.rotation))
                            .start()
                        attemps++
                    }
                }
            }
            return@setOnTouchListener true
        }
        poly2.setOnTouchListener { view, motionEvent ->
            val action = motionEvent.actionMasked
            val cnt = motionEvent.pointerCount
            if (action == MotionEvent.ACTION_MOVE) {
                if (cnt==2) {
                    var result = atan2(center-motionEvent.getY(1), motionEvent.getX(1)-center)*180/3.14f
                    result=angle-result+lastRot2
                    Log.d("errror", "move $result, ${motionEvent.getY(1)}")
                    polyWHT2.animate()
                        .setDuration(0)
                        .rotation(result%360)
                        .start()
                } else {
                    if (!aboba) {
                        poly2.animate()
                            .setDuration(0)
                            .translationXBy(motionEvent.x - x)
                            .translationYBy(motionEvent.y - y)
                            .start()
                    } else {
                        x=motionEvent.x
                        y=motionEvent.y
                        aboba=false
                    }
                }
            } else if (action == MotionEvent.ACTION_DOWN) {
                aboba=false
                x=motionEvent.x
                y=motionEvent.y
            } else if (action == MotionEvent.ACTION_POINTER_DOWN) {
                angle= atan2(center-motionEvent.getY(1), motionEvent.getX(1)-center)*180/3.14f
                aboba=true
            } else if (action==MotionEvent.ACTION_UP) {
                lastRot2=polyWHT2.rotation
                Log.d("errror", "up ${poly2.x-place2.x+50}, ${poly2.y}, ${abs((polyWHT2.rotation-place2.rotation)%350)}")
                if (abs(poly2.x-place1.x+50)<50 && abs(poly2.y)<50 ||
                    abs(poly2.x-place3.x+50)<50 && abs(poly2.y)<50) {
                    poly2.animate()
                        .setDuration(400)
                        .translationX(0f)
                        .translationY(350f)
                        .start()
                    polyWHT2.animate()
                        .setDuration(400)
                        .rotation(0f)
                        .start()
                    attemps++
                } else if (abs(poly2.x-place2.x+50)<50 && abs(poly2.y)<50) {
                    if (abs((polyWHT2.rotation-place2.rotation)%355)<10f) {
                        poly2.isEnabled = false
                        poly2.animate()
                            .setDuration(200)
                            .translationX(0f)
                            .translationY(0f)
                            .start()
                        polyWHT2.animate()
                            .setDuration(200)
                            .rotationBy((720+place2.rotation-polyWHT2.rotation)%360)
                            .withEndAction {
                                place2.animate()
                                    .setDuration(400)
                                    .alpha(0f)
                                    .withEndAction {
                                        polyWHT2.animate()
                                            .setDuration(200)
                                            .alpha(0f)
                                            .start()
                                        cntt--
                                        if (cntt==0) {
                                            endGame()
                                        }
                                    }
                                    .start()
                            }
                            .start()
                    } else {
                        poly2.animate()
                            .setDuration(400)
                            .translationX(0f)
                            .translationY(350f)
                            .start()
                        polyWHT2.animate()
                            .setDuration(400)
                            .rotation(ZeroOr360(polyWHT2.rotation))
                            .start()
                        attemps++
                    }
                }
            }
            return@setOnTouchListener true
        }
        poly3.setOnTouchListener { view, motionEvent ->
            val action = motionEvent.actionMasked
            val cnt = motionEvent.pointerCount
            if (action == MotionEvent.ACTION_MOVE) {
                if (cnt==2) {
                    var result = atan2(center-motionEvent.getY(1), motionEvent.getX(1)-center)*180/3.14f
                    result=angle-result+lastRot3
                    Log.d("errror", "move $result, ${motionEvent.getY(1)}")
                    polyWHT3.animate()
                        .setDuration(0)
                        .rotation(result%360)
                        .start()
                } else {
                    if (!aboba) {
                        poly3.animate()
                            .setDuration(0)
                            .translationXBy(motionEvent.x - x)
                            .translationYBy(motionEvent.y - y)
                            .start()
                    } else {
                        x=motionEvent.x
                        y=motionEvent.y
                        aboba=false
                    }
                }
            } else if (action == MotionEvent.ACTION_DOWN) {
                aboba=false
                x=motionEvent.x
                y=motionEvent.y
                Log.d("errror", "$x, $y")
            } else if (action == MotionEvent.ACTION_POINTER_DOWN) {
                angle= atan2(center-motionEvent.getY(1), motionEvent.getX(1)-center)*180/3.14f
                aboba=true
            } else if (action==MotionEvent.ACTION_UP) {
                Log.d("errror", "up ${poly3.x-place3.x+50}, ${poly3.y}, ${abs((polyWHT3.rotation-place3.rotation)%350)}")
                lastRot3=polyWHT3.rotation
                if (abs(poly3.x-place1.x+50)<50 && abs(poly3.y)<50 ||
                    abs(poly3.x-place2.x+50)<50 && abs(poly3.y)<50) {
                    poly3.animate()
                        .setDuration(400)
                        .translationX(0f)
                        .translationY(350f)
                        .start()
                    polyWHT3.animate()
                        .setDuration(400)
                        .rotation(0f)
                        .start()
                    attemps++
                } else if (abs(poly3.x-place3.x+50)<50 && abs(poly3.y)<50) {
                    if (abs((polyWHT3.rotation-place3.rotation)%355)<10f) {
                        poly3.isEnabled = false
                        poly3.animate()
                            .setDuration(200)
                            .translationX(0f)
                            .translationY(0f)
                            .start()
                        polyWHT3.animate()
                            .setDuration(200)
                            .rotationBy((720+place3.rotation-polyWHT3.rotation)%360)
                            .withEndAction {
                                place3.animate()
                                    .setDuration(400)
                                    .alpha(0f)
                                    .withEndAction {
                                        polyWHT3.animate()
                                            .setDuration(200)
                                            .alpha(0f)
                                            .start()
                                        cntt--
                                        if (cntt==0) {
                                            endGame()
                                        }
                                    }
                                    .start()
                            }
                            .start()
                    } else {
                        poly3.animate()
                            .setDuration(400)
                            .translationX(0f)
                            .translationY(350f)
                            .start()
                        polyWHT3.animate()
                            .setDuration(400)
                            .rotation(ZeroOr360(polyWHT3.rotation))
                            .start()
                        attemps++
                    }
                }
            }
            return@setOnTouchListener true
        }
    }

    fun ZeroOr360(f: Float): Float {
        return if (abs(f)%360>180) { 360f } else { 0f }
    }

    companion object {
        lateinit var root: Activity
        fun smt() {
            updateLeaderBoard()
            if (!root.button.isClickable) {
                root.button.animate()
                    .setDuration(400)
                    .translationY(0f)
                    .start()
                root.top5.animate()
                    .setDuration(400)
                    .translationX(0f)
                    .start()
            }
            root.button.isClickable=true
            root.place1.visibility= View.GONE
            root.place2.visibility= View.GONE
            root.place3.visibility= View.GONE
            root.poly1.visibility= View.GONE
            root.poly2.visibility= View.GONE
            root.poly3.visibility= View.GONE
        }

        fun updateLeaderBoard() {
            val sh = root.getSharedPreferences("MySuperGame", 0)
            for (i in 1..5) {
                var time = SimpleDateFormat("mm:ss").format(sh.getLong("time$i", -1))
                var score = sh.getInt("score$i", -1).toString()
                var name = sh.getString("name$i", "Anon")
                if (score=="-1") {
                    time = "---"
                    score = "---"
                    name = "---"
                }
                if (i==1) {
                    root.name1.text = name
                    root.time1.text = time
                    root.attemps1.text = score
                } else if (i==2) {
                    root.name2.text = name
                    root.time2.text = time
                    root.attemps2.text = score
                } else if (i==3) {
                    root.name3.text = name
                    root.time3.text = time
                    root.attemps3.text = score
                } else if (i==4) {
                    root.name4.text = name
                    root.time4.text = time
                    root.attemps4.text = score
                } else {
                    root.name5.text = name
                    root.time5.text = time
                    root.attemps5.text = score
                }
            }
        }
    }
}