package com.example.puzzle

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.semantics.SemanticsProperties.Text
import java.nio.file.WatchEvent

class MyTriangle @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : View(context, attrs, defStyleAttr) {
    lateinit var list: List<Pair<Float, Float>>
    lateinit var style: Paint.Style

    fun newTriangle(list: List<Pair<Float, Float>>, style : Paint.Style) {
        this.list = list
        this.style = style
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        val paint = Paint(Color.BLUE)

        val path = Path()
        paint.reset()
        paint.strokeWidth=5f
        paint.style = style
        if (style==Paint.Style.FILL) { paint.color = Color.LTGRAY }
        else {
            paint.color = Color.GREEN
            path.moveTo(list[0].first, list[0].second)
            for (i in list) {
                path.lineTo(i.first, i.second)
            }
            path.lineTo(list.first().first, list.first().second)
            canvas!!.drawPath(path, paint)
            paint.color = Color.WHITE
            paint.style = Paint.Style.FILL
        }
        path.moveTo(list[0].first, list[0].second)
        for (i in list) {
            path.lineTo(i.first, i.second)
        }
        path.lineTo(list.first().first, list.first().second)
        canvas!!.drawPath(path, paint)
    }
}