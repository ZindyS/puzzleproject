package com.example.puzzle

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.DialogFragment
import kotlinx.android.synthetic.main.dialog.*
import kotlinx.android.synthetic.main.dialog.view.*
import java.text.SimpleDateFormat
import java.util.*

class CustomAlert(val score: Int, val time : Date) : DialogFragment() {
    lateinit var root : View

    override fun onStart() {
        super.onStart()
        val width = (resources.displayMetrics.widthPixels*0.5).toInt()
        val height = (resources.displayMetrics.heightPixels*0.8).toInt()
        dialog!!.window!!.setLayout(width, height)
        dialog!!.setCancelable(false)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        root = inflater.inflate(R.layout.dialog, container, false)
        root.time.text = SimpleDateFormat("mm:ss").format(time)
        root.score.text = score.toString()
        root.editTextTextPersonName.addTextChangedListener {
            if (it!!.toString()=="") {
                root.textView5.text = "Сохранить анонимно"
            } else {
                root.textView5.text = "Сохранить"
            }
        }
        root.textView5.setOnClickListener {
            val sh = root.context.getSharedPreferences("MySuperGame", 0)
            val ed = sh.edit()
            var str = editTextTextPersonName.text.toString()
            if (str == "") {
                str = "Anon"
            }
            for (i in 1..5) {
                if (sh.getLong("time$i", 9999999)>time.time ||
                    (sh.getLong("time$i", 9999999)==time.time &&
                            sh.getInt("score$i", 999999)>score)) {
                    var lastName=str
                    var lastScore=score
                    var lastTime=time.time
                    for (j in i..5) {
                        var tmpN=sh.getString("name$j", "---")
                        var tmpS=sh.getInt("score$j", -1)
                        var tmpTime=sh.getLong("time$j", -1)
                        ed.putLong("time$j", lastTime)
                        ed.putInt("score$j", lastScore)
                        ed.putString("name$j", lastName)
                        ed.commit()
                        lastName = tmpN!!
                        lastScore = tmpS
                        lastTime = tmpTime
                    }
                    break
                }
            }
            MainActivity.smt()
            dialog!!.cancel()
        }
        return root
    }
}